/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresa;

import static Login.Menu.confirmarFecha;
import Validaciones.Validacion;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Romy
 */
public class SistemaPromocion {
    
    Scanner sc = new Scanner(System.in);
    private List<Empresa> empresas = new ArrayList<>();
    private List<Establecimiento> establecimientos = new ArrayList<>();
    private List<Promocion> promociones = new ArrayList<>();
    
    
    public void CreacionEmpresa() {
        System.out.print("\nIngresar nombre de la Empresa: ");
        String nombre = sc.nextLine();
        if (consultarEmpresa(nombre) == null) {
            System.out.print("Ingresar la Categoria ");
            String categoria = sc.nextLine();
            
            System.out.print("Ingresar su URL ");
            String urlEmpresa = sc.nextLine();
            
            System.out.print("Ingresar su Usuario Facebook ");
            String usuarioFace = sc.nextLine();
            
            System.out.print("Ingresar su Usuario twitter ");
            String usuarioTwitter = sc.nextLine();
            
            System.out.print("Ingresar su Usuario Instagram ");
            String usuarioInsta = sc.nextLine();
            
            empresas.add(new Empresa(nombre, categoria,urlEmpresa,usuarioFace,usuarioTwitter,usuarioInsta));
            System.out.println("¡Su Empresa fue creada!\n");
        } else {
            System.out.println("\nYa existe una Empresa con este nombre");
        }
    }
     public Empresa consultarEmpresa(String empre) {
        for (Empresa d : empresas) {
            if (d.getNombreEmpresa().equals(empre)) {
                return d;
            }
        }
        return null;
    }
     
     
      public void CreacionEstablecimiento() {
        System.out.print("Ingresar la Empresa a la que pertenece su Establecimiento:");
        String empreEsta = sc.nextLine();

        Empresa de = consultarEmpresa(empreEsta);

        if (de == null) {
            System.out.println("No existe esa Empresa");
        } else {
            System.out.print("\nIngresar la direccion del establecimiento: ");
            String direccionEsta = sc.nextLine();
            if (consultarEstablecimiento(direccionEsta) == null) {
                
                Sector s=menuSector();
                System.out.print("Ingrese ciudad: ");
                String ciudad=sc.next();
                sc.nextLine();
                String[] telefono=Validacion.pedirTelefono();
                System.out.println("Ingrese horario de atencion");
                String horarioAten[]=Validacion.pedirHorario();
                establecimientos.add(new Establecimiento(ciudad,s, direccionEsta,telefono,horarioAten,de));
                System.out.println("¡Su Establecimiento fue creado!\n");
            } else {
                System.out.println("\nYa existe un establecimiento con esta direccion");
            }
        }
    }
    
       /**
     * Menu para el ingreso del sector
     * @return Sector
     */
      public Sector menuSector(){
        String opcion="";
        while(!opcion.equals("4")){
            System.out.println("\n*********** Menu *************");
            System.out.println("1."+Sector.NORTE);
            System.out.println("2."+Sector.SUR);
            System.out.println("3."+Sector.ESTE);
            System.out.println("4."+Sector.OESTE);
            System.out.print("\nIngrese el sector: ");
            opcion = sc.next();
            switch (opcion){
                case "1":
                    return Sector.NORTE;

                case "2":
                   return Sector.SUR;

                case "3":
                    return Sector.ESTE;
              
                case "4":
                    return Sector.OESTE;
                default:
                    System.out.println("Opcion No valida!!");
            }
        }
     return null;
    
    }  
    
      /**
     * Busca si una objeto Establecimiento existe
     * @param esta String
     * @return Etapa
     */
    public Establecimiento consultarEstablecimiento(String esta) {
        for (Establecimiento d : establecimientos) {
            if (d.getDireccion().equals(esta)) {
                return d;
            }
        }
        return null;
    }
     
    /**
     * Crea una promocion y la guarda en la lista de promociones
    */
    
    
    public void CreacionPromocion() {
        System.out.print("Ingresar el Establecimiento que tendra esta promocion:");
        String empreEsta = sc.nextLine();

        Establecimiento de = consultarEstablecimiento(empreEsta);

        if (de == null) {
            System.out.println("No existe este Establecimiento");
        } else {
            System.out.print("\nIngresar la descripcion de la promocion: ");
            String descri = sc.nextLine();
            System.out.println("Ingrese fecha de inicio de la promocion formato yyyy-MM-dd");
            String fecha11 = sc.next();
            while(!confirmarFecha(fecha11)==true){
                System.out.println("Escriba correctamente  fecha de inicio de la promocion formato yyyy-MM-dd");
                fecha11=sc.next();  
                sc.nextLine();

            }
            LocalDate fechainicio=LocalDate.parse(fecha11);
            System.out.println("Ingrese fecha de fin de la promocion formato yyyy-MM-dd");
            String fecha22= sc.next();
            sc.nextLine();
            while(!confirmarFecha(fecha22)==true){
                System.out.println("Escriba correctamente fecha de fin de la promocion formato yyyy-MM-dd");
                fecha22=sc.next();  
                sc.nextLine();

            }
            LocalDate fechafin=LocalDate.parse(fecha22);
             System.out.print("Ingrese la tarjeta que tiene el beneficio: ");
             String tarjetabeneficio=sc.next();
                sc.nextLine();

           
            if (consultarPromocion(de,descri,fechainicio,fechafin,tarjetabeneficio) == null) {
                
              
                promociones.add(new Promocion(descri,fechainicio,fechafin,tarjetabeneficio,de));
                System.out.println("**************¡Su Promocion fue creada!*************\n");
            } else {
                System.out.println("\nYa existe una Promocion con iguales datos");
            }
        }
    }
    
    /**
     * Busca si una objeto Promocion existe
     * @param e
     * @param promo
     * @param fechain
     * @param fechafin
     * @param tarjeta
     * @return Promocion
     */
    public Promocion consultarPromocion(Establecimiento e,String promo,LocalDate fechain,LocalDate fechafin,String tarjeta) {
        for (Promocion p : promociones) {
             if(e.getDireccion().equals(p.getEstablecimiento().getDireccion())&&p.getDescripcion().equals(promo)&&p.getFechaInicio().equals(fechain)){
                if (p.getFechaFin().equals(fechafin)) {
                    if(p.getTarjeta().equals(tarjeta)){
                    return p;}
                }
            }
        }
        return null;
    }
    

    
    
}
