/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresa;

import java.time.LocalDate;
import static java.time.temporal.ChronoUnit.DAYS;

/**
 *
 * @author Romy
 */
public class Promocion {
    //VARIABLES DE CLASE
    private static int contpromo=0;
    //VARIABLES DE INSTANCIA 
    private final int codigoPromocion;
    private String descripcion;
    private long diaValidez;// creo que es una lista
    private LocalDate fechaInicio;
    private LocalDate fechaFin;
    private String tarjeta;// por ahora es para una sola tarjeta 
    private Establecimiento establecimiento;

    public Promocion( String descripcion, LocalDate fechaInicio, LocalDate fechaFin, String tarjeta, Establecimiento establecimiento) {
        this.codigoPromocion = this.generarIdpromo();
        this.descripcion = descripcion;
        this.diaValidez = Promocion.devolverDias_validez(fechaInicio,fechaFin);
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.tarjeta = tarjeta;
        this.establecimiento = establecimiento;
    }

    public int getCodigoPromocion() {
        return ++Promocion.contpromo;
    }
    
      private int generarIdpromo(){
        return ++Promocion.contpromo; //Creamos el codigo para promocion
        
    }

    public String getDescripcion() {
        return descripcion;
    }

    public long getDiaValidez() {
        return diaValidez;
    }

    public LocalDate getFechaInicio() {
        return fechaInicio;
    }

    public LocalDate getFechaFin() {
        return fechaFin;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public Establecimiento getEstablecimiento() {
        return establecimiento;
    }
    
    public static long devolverDias_validez(LocalDate inicio,LocalDate fin){
   
                long dias_validez = DAYS.between(inicio,fin);
                System.out.println("Hay "+dias_validez+" dias de diferencia");
                return dias_validez;
 
		
 
		
    }
    
    
    
    
}
