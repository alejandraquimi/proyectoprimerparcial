/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresa;

import Login.Administradores;
import java.util.ArrayList;

/**
 *
 * @author Romy
 */
public class Empresa {
    //VARIABLES DE CLASE
    private static int contempre=0;
    //VARIABLES DE INSTANCIA
    private final int codigoEmpresa;
    private String nombreEmpresa;
    private String categoria;   
// private ArrayList<Categoria> categoria;
    private String url;
    private String facebook;
    private String twitter;
    private String instagram;
   // private ArrayList<Establecimiento>establecimientos;
    
    //BLOQUE DE CODIGO : ME SIRVE PARA INICIALIZAR ALGUNA VARIABLE ANTES DE LLAMAR AL CONTRUCTOR
    //SERIA PARA GENERAR EL CODIGO 

    public Empresa( String nombreEmpresa,String categoria, String url, String facebook, String twitter, String instagram) {
        this.codigoEmpresa = this.generarIdempre(); //This hace referencia al objeto actual 
        this.nombreEmpresa = nombreEmpresa;
        this.categoria = categoria;
        this.url = url;
        this.facebook = facebook;
        this.twitter = twitter;
        this.instagram = instagram;
    }
    
    // AUMENTAS EL VALOR 

    public int getCodigoEmpresa() {
        return ++Empresa.contempre;// No se que hace este método aun
    }
    private int generarIdempre(){
        return ++Empresa.contempre; // Este metodo me crea codigo de la empresa y a la vez aumenta su valor
        
    }
   

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public String getCategoria() {
        return categoria;
    }

    public String getUrl() {
        return url;
    }

    public String getFacebook() {
        return facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public String getInstagram() {
        return instagram;
    }
    
    
    
    
    
    
    
    
}
