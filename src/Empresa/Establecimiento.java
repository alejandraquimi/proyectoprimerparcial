/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresa;

import Login.Administradores;
import java.util.ArrayList;

/**
 *
 * @author Romy
 */
public class Establecimiento {
    //Variables de clase
    private static int cont_esta=0;
    //Variables de Instancia
    private final int codigoEstablecimiento;
    private String ciudad;
    private Sector sector;
    private String direccion;
    private String[] telefonos;
    private String[] horarioAtencion;
    private Empresa empresa;
    //private ArrayList<Promocion>promociones;

    public Establecimiento( String ciudad, Sector sector, String direccion, String[] telefonos, String[] horarioAtencion, Empresa empresa) {
        this.codigoEstablecimiento = this.generarIdesta();
        this.ciudad = ciudad;
        this.sector = sector;
        this.direccion = direccion;
        this.telefonos = telefonos;
        this.horarioAtencion = horarioAtencion;
        this.empresa = empresa;
    }

    public int getCodigoEstablecimiento() {
        return ++Establecimiento.cont_esta; // No se que hace este metodo
    }
      private int generarIdesta(){
        return ++Establecimiento.cont_esta; // Este metodo me crea codigo para el establecimiento y a la vez aumenta su valor
        
    }
   

    public String getCiudad() {
        return ciudad;
    }

    public Sector getSector() {
        return sector;
    }

    public String getDireccion() {
        return direccion;
    }

    public String[] getTelefonos() {
        return telefonos;
    }

    public String[] getHorarioAtencion() {
        return horarioAtencion;
    }

    public Empresa getEmpresa() {
        return empresa;
    }
    
    
    
    
    
}
