/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Login;

import Empresa.Empresa;
import Empresa.Promocion;
import Empresa.SistemaPromocion;
import Validaciones.Validacion;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 *
 * @author Romy
 */
public  class Menu {
    SistemaPromocion Aplicacion = new SistemaPromocion();// me imagino que creas una promocion
    private ArrayList<Persona> per= new ArrayList();//AQUI ESTA EL ADMINISTRADOR Y USUARIO
    private ArrayList<Empresa> empresas = new ArrayList();
    private ArrayList<Promocion> promociones=new ArrayList();
    // Usuario y Administrador de Ejemplo 
    Administradores administrador1=new Administradores("Jose Carlos Lopez","jocarlop","azul98");
    //Usuario usuario1=new Usuario(12,"21-12-1998","Pasaje",["visa","masterCard"],"Adrian Perez","adripe77","blue44");
    //per.add(administrador1);
    private Validacion metodoValida= new Validacion();

    public Scanner sc=new Scanner(System.in);

    Administradores admin;
    public Menu(){
        agregar();
    }
           
    //MENU PRINCIPAL   
    public void menu(){
        String opcion="";
        while(!opcion.equals("3")){
            System.out.println("\n╔                Menu                 ╗");
            System.out.println("║ 1. Iniciar Sesion                     ║");
            System.out.println("║ 2. Registrarse                        ║");
            System.out.println("║ 3. Salir                              ║");
            System.out.println("╚                                       ╝");
            System.out.print("\nIngrese opcion: ");
            opcion = sc.next();
            switch (opcion){
                case "1":
                    login1();
                    break;
                case "2":
                    login2();
                    break;
                case "3":
                    System.out.println("Desea salir del sistema (Si/No)");
                    String valor=sc.next();
                    if (valor.toLowerCase().equals("si")){
                        opcion="3";
                        break;
                    }
                    else{
                        opcion="";
                        break;
                    }    
                default:
                    System.out.println("Opcion No valida!!");
                    return;
            }
        }
    }
    
    //METODO PARA LA OPCION UNO EN ESTE MENU TAMBIEN ESTARA COMPUESTO POR OTROS QUE SE HAN CREADO MAS ABAJO    
    public void login1(){
        
                System.out.print("\nIngrese su usuario: ");
                String usuario = sc.next();
                sc.nextLine();
                System.out.println("\nIngrese contraseña");
                String contraseña=sc.next();
                sc.nextLine();
                conultarUsuario(usuario,contraseña);
        }
    
    //METODO CONSULTAR USUARIO
     public void conultarUsuario(String usuario,String contra) {
        for(Persona persona:per){
           if(persona.getCorreo().equalsIgnoreCase(usuario)&&persona.getPassword().equals(contra)){
               //Segumis
               if(persona instanceof Usuario){
                   System.out.println("Bienvenido Usuario");
                   Usuario usuarioOpcion=(Usuario)persona;
                   menuHabiente(usuarioOpcion);
           }
               else if(persona instanceof Administradores){
                   System.out.println("Bienvenido Administrador");
                   Administradores adminisOpcion=(Administradores)persona;
                   menuAdmi(adminisOpcion);
           } 
           }
           else{
               System.out.println("Usuario o Contraseña incorrecta");
               System.out.println("Ingrese denuevo");
               return;
           }
        }
        }
    //MENU QUE MUESTRA EL MENU DEL USUARIO HABIENTE ESTE ESTA INCLUIDO EN LOGIN1() EN LA OPCION 1
    public void menuHabiente(Usuario usuario){
        //if(persona instanceof Usuario){
          //  Usuario objetoHabiente=(Usuario)persona;
        
        String opcionHabiente="";
        while(!opcionHabiente.equals("4")){
            System.out.println("*******BIENVENIDO USUARIOHABIENTE*********");
            System.out.println("\n╔                Menu                 ╗");
            System.out.println("║ 1. Mi cuenta                          ║");
            System.out.println("║ 2. Consultar empresa                  ║");
            System.out.println("║ 3. Consultar promociones              ║");
            System.out.println("║ 4. Cerrar Sesión                      ║");
            System.out.println("╚                                       ╝");
            System.out.print("\nIngrese opcion: ");
            opcionHabiente = sc.next();
            switch (opcionHabiente){
                case "1":
                    miCuentaUsuario(usuario);
                    break;
                case "2":
                    consultarEmpresaUsuario(usuario);
                    break;
                case "3":
                    break;
                case "4":
                    opcionHabiente="4";
                    break;
                default:
                    System.out.println("Opcion No valida!!");
                    System.out.println("Ingrese una opcion válida");
            }        
        }
        
    }
    
    //MENU QUE ESTA INCLUIDO EN EL MENUHABIENTE() DE ABAJO, ADEMAS AQUI ES DONDE SE REALIZARAN LOS CAMBIOS
    //QUE EL USUARIO DESEA MODIFICAR SUS DATOS
    public void miCuentaUsuario(Usuario objeto) {
        String opcion="";
        while(!opcion.equals("6")){
        System.out.print("\n1. nombre:"+objeto.getNombreCompleto()+""
                +"\n2. correo:"+objeto.getCorreo()+"\n3. Fecha de Nacimiento:"+objeto.getFechaNacimiento()+
                "\n4. Ciudad de Residencia:"+objeto.getCiudadNacimiento()+
                "\n5. Tarjetas:"+Arrays.toString(objeto.getTipoTarjeta())+"\n6. Contraseña:"+objeto.getPassword()+
                "\nIngrese la opcion que desea modificar:");
        opcion = sc.next().toLowerCase();
        switch (opcion){
            case("1"):
                System.out.println("Ingrese nombre:");
                objeto.setNombreCompleto(sc.next());
                break;
            case("2"):
                System.out.println("Ingrese su Correo:");
                objeto.setCorreo(sc.next());
                break;
            case("3"):
                System.out.println("Ingrese Fecha de Nacimiento:");
                String fecha = sc.nextLine();
            while(!confirmarFecha(fecha)==true){ //Se utiliza una Excepcion
                System.out.println("Escriba correctamente su fecha de nacimiento");
                fecha=sc.nextLine();            
            }
            LocalDate fechaMod=LocalDate.parse(fecha);
            objeto.setFechaNacimiento(fechaMod);          
                break;
            case("4"):
                System.out.println("Ingrese Ciudad de Residencia: ");
                String ciudad=sc.next();
                objeto.setCiudadNacimiento(ciudad);
                break;
            case("5"):
                System.out.println("Ingrese Tarjetas(Separada ',': ");
                String arreglo=sc.next();
                String[] arregloTarjeta = arreglo.split(",");
                objeto.setTipoTarjeta(arregloTarjeta);
                break;
 
            case("6"):
                System.out.println("Ingrese su nueva coontraseña: ");
                String contrasena=sc.next();
                objeto.setPassword(contrasena);
                break;
             default:
                System.out.println("Opcion no valida");
                System.out.println("Por favor elija una opcion válida");
    }
        }
        System.out.println("Desea seguir modificando?SI/NO");
        String respuesta=sc.next();
        if(respuesta.toLowerCase().equals("si")){
        miCuentaUsuario(objeto);// Volviendo a llamar al metodo
        }
        return;
    }
    
    //METODO PARA CONSULTAR EMPRESA
    public void consultarEmpresaUsuario(Usuario usuario){
        String opcion="";
        while(!opcion.equals("5")){
            System.out.println("Consulta de Empresas"+"\n"+"========="+"\n"+"  1. Todos"+"\n"
                +"  2. Por nombre"+"\n"+"  3. Por categoría"+"\n"+"  4. Por ciudad y sector"+"\n"
                +"  5.Regresar al menu principal");
            opcion = sc.next().toLowerCase();
            switch (opcion){
            case("1"):
                int numeroEmpresas = empresas.size();
                Empresa[] empresasExistentes = new Empresa[numeroEmpresas];
                for(int cont=0;cont<numeroEmpresas;cont++){
                    empresasExistentes[cont]=empresas.get(cont);                    
                }
                System.out.println(empresasExistentes);
                break;
            case("2"):
               
                break;
            case("3"):
                                    
                break;
            case("4"):
                
                break;
            case("5"):
                System.out.println("Seguro/a que desea regresar a la opcion principal");
                String respuesta = sc.next().toLowerCase();
                if(respuesta=="si"){
                    break;
                }
                else if(respuesta=="no")
                    return;
                
            default:
                System.out.println("Opcion no valida");
                System.out.println("Por favor elija un opcion valida");
        }
    }
    }
    
    public void consultarPromocionesUsuario(Usuario usuario){
        String opcion="";
        while(!opcion.equals("5")){
            System.out.println("Consulta de Empresas"+"\n"+"========="+"\n"+"  1. Todos"+"\n"
                +"  2. Por descripcion"+"\n"+"  3. Por dias de validez"+"\n"+"  4. Por tarjeta"+"\n"
                +"  5.Regresar al menu principal");
            opcion = sc.next().toLowerCase();
            switch (opcion){
            case("1"):
                int numeroPromocion = promociones.size();
                Promocion[] promocionesExistentes = new Promocion[numeroPromocion];
                for(int cont=0;cont<numeroPromocion;cont++){
                    promocionesExistentes[cont]=promociones.get(cont);                    
                }
                System.out.println(promocionesExistentes);
                break;
            case("2"):
                System.out.println("Por favor escriba el nombre de la Empresa que desea consultar");
                String opcionNombre=sc.nextLine().toLowerCase();
                
                break;
            case("3"):
                System.out.println("Por favor escriba la categoria de la Empresa que desea cosultar");
                String opcionCategoria=sc.nextLine().toLowerCase();
                                    
                break;
            case("4"):
                System.out.println("Por favor escriba la ciudad o sector donde se ecnuantra la empresa");
                String opcionCiudadSector=sc.nextLine().toLowerCase();
                
                
                break;
            case("5"):
                System.out.println("Seguro/a que desea regresar a la opcion principal");
                String respuesta = sc.next().toLowerCase();
                if(respuesta=="si"){
                    break;
                }
                else if(respuesta=="no")
                    return;
                
            default:
                System.out.println("Opcion no valida");
                System.out.println("Por favor elija un opcion valida");
        }
    }
    }
    
    
    //MENU QUE MUESTRA EL MENU DEL USUARIO Administrador ESTE ESTA INCLUIDO EN LOGIN1() EN LA OPCION 2 
    public void menuAdmi(Administradores persona) {
        //if(persona instanceof Administradores){
        //Administradores adminis=(Administradores)persona; 
        String opcionAdmi="";
        while(!opcionAdmi.equals("5")){
            System.out.println("********BIENVENIDO ADMINISTRADOR**********");
            System.out.println("\n╔       Menu Principal                ╗");
            System.out.println("║ 1. Mi cuenta                          ║");
            System.out.println("║ 2. Registrar empresa                  ║");
            System.out.println("║ 3. Registrar establecimiento          ║");
            System.out.println("║ 4. Registrar promocion                ║");
            System.out.println("║ 5. Cerrar Sesión                      ║");
            System.out.println("╚                                       ╝");
            System.out.print("\nIngrese opcion: ");
            opcionAdmi = sc.next();
            switch (opcionAdmi){
                case "1":
                    loginAdministrador(persona);
                    break;
                case "2":
                    System.out.println("Registra empresa");
                    Aplicacion.CreacionEmpresa();
                    break;
                case "3":
                    System.out.println("Registra establecimiento");
                    Aplicacion.CreacionEstablecimiento();
                    break;
                case "4":
                    System.out.println("Registra promocion");
                    Aplicacion.CreacionPromocion();
                    
                    break;
                case "5":
                    opcionAdmi="5";
                    break;
                
                default:
                    System.out.println("Opcion No valida!!");
      
            }        
        }
         
    }
    
    
    
    //MENU PARA LA OPCION 2    
    String correo;
    public void login2() {
            int i=0;
        System.out.println("Escriba su Nombre ");
        String nombreConsulta=sc.next();

        System.out.println("*********Escriba su dirección de correo********* ");
         correo=sc.next();
         
         System.out.println("Ingrese fecha de nacimiento con formato yyyy-MM-dd");
            String fecha = sc.next();

            while(!confirmarFecha(fecha)==true){
                System.out.println("Escriba correctamente su fecha de nacimiento");
                fecha=sc.next();            
            }
            LocalDate fecha1=LocalDate.parse(fecha);
            System.out.println("\nIngrese Ciudad de Residencia");
            String ciudadR = sc.next();
            System.out.println("\nIngrese las tarjetas que tiene separadas por ¨,¨ ");
            String arreglo=sc.next();
            String[] arregloTarjeta = arreglo.split(",");
            System.out.println("\nIngrese contraseña");
            String contraseña = sc.next();
            System.out.print("\nConfirme su contraseña");
            String confirmacion = sc.next();
            while(!(contraseña.equals(confirmacion))){
                System.out.println("Escriba correctamente su contraseña ");
                confirmacion=sc.next();   
            }
            //fechaNacimiento, String ciudadNacimiento, String[] tipoTarjeta, int Codigo, String nombreCompleto, String Correo, String Password
            
            
            //i=u.getCodigo();

            per.add(new Usuario(i,fecha1,ciudadR,arregloTarjeta,nombreConsulta,correo,contraseña));
            System.out.println("\nUsuario creado exitosamente\nAcceda al sistema nuevamente");

                 
        }
    //CREO EL ADMINISTRADOR UNICO Y LO AGREGO A LA LISTA DE PERSONAS    
    public  void agregar(){
           
            per.add(new Administradores("alejandra","usuario","usuario"));
            
        }
        

    
    //CONFIRMA QUE INGRESES BIEN LA FECHA    
    public static boolean confirmarFecha(String fecha){
         try{
             LocalDate hora = LocalDate.parse(fecha);
             System.out.println(hora);
             return true;
        }catch(Exception e){
           return false;
        }
    }
       
  

        


    
    

    //DONDE SE MODIFICA DATOS ADMINISTRADOR ESTE ES USADO EN EL MENUADMI() OPCION1 
    public void loginAdministrador(Administradores objeto) {
        String opcion="";
        while(!opcion.equals("5")){
         
        System.out.print("\n1. nombre:"+objeto.getNombreCompleto()+""
                +"\n2. correo:"+objeto.getCorreo()+"\n3.Contraseña:"+objeto.getPassword()+
                "\nIngrese la opcion que desea modificar:");
        opcion = sc.next().toLowerCase();
        switch (opcion){
            case("1"):
                System.out.println("Ingrese nombre:");
                objeto.setNombreCompleto(sc.next());
                break;
            case("2"):
                System.out.println("correo:");
                objeto.setCorreo(sc.next());
                break;
            case("3"):
                System.out.println("Ingrese su nueva coontraseña: ");
                String contraseña=sc.next();
                objeto.setPassword(contraseña);
                break;
             default:
                System.out.println("Opcion no valida");
                
            
        
    }
        
        System.out.println("Desea seguir modificando?SI/NO");
        String respuesta=sc.next();
        if(respuesta.toLowerCase().equals("si")){
         loginAdministrador(objeto);
           
        }else{
            break;
        }
        }
    }
    //DEVUELVE LA LISTA DEPENDIENDO SI ES ADMINISTRADOR O HABIENTE
    public void devolverListaPersona(String persona){
        if (persona.equalsIgnoreCase("habiente")){
            System.out.println( per.stream().filter(p ->p instanceof Usuario ).collect(Collectors.toList()));
        }if(persona.equalsIgnoreCase("admi")){
            System.out.println( per.stream().filter(p ->p instanceof Administradores).collect(Collectors.toList()));

        }
        ///holaaaaaaa
        System.out.println("dfg");
            
}

    public ArrayList<Persona> getPer() {
        return per;
    }

    public void setPer(ArrayList<Persona> per) {
        this.per = per;
    }

    public ArrayList<Empresa> getEmpresas() {
        return empresas;
    }

    public void setEmpresas(ArrayList<Empresa> empresas) {
        this.empresas = empresas;
    }

  
}
        


    

