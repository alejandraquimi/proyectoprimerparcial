/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Login;

import Empresa.Categoria;
import Empresa.Sector;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;

/**
 *
 * @author Romy
 */
public class Usuario extends Persona{
    //private int codigoU;
    
    private LocalDate fechaNacimiento;
    private String ciudadNacimiento;
    private String[] tipoTarjeta; 
    private static int cont=0;
    private final int Codigo;


    public Usuario(int Codigo,LocalDate fechaNacimiento, String ciudadNacimiento, String[] tipoTarjeta,String nombreCompleto, String Correo, String Password) {
        super(nombreCompleto, Correo, Password);
        this.fechaNacimiento = fechaNacimiento;
        this.ciudadNacimiento = ciudadNacimiento;
        this.tipoTarjeta = tipoTarjeta;
        this.Codigo=this.generarId();
    }
    

    

    /*public int getCodigoU() {
        return codigoU;
    }*/

   

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public String getCiudadNacimiento() {
        return ciudadNacimiento;
    }

    public String[] getTipoTarjeta() {
        return tipoTarjeta;
    }
    public  void presentarDatos(){
        System.out.println("Password.\n"+
                "TarjetaHabiente: "+getCodigo()+
                ".\nNombre Completo: "+getNombreCompleto()+
                " Correo: "+getCorreo()+"Password.\n" +
                getPassword()+"Fecha de Nacimiento.\n" +getFechaNacimiento()+"Ciudad de Nacimiento\n" 
                +getCiudadNacimiento() +"Tajetas:"+Arrays.toString(getTipoTarjeta())+"\n");
    
    }
    
    public String consultarEmpresasNombre(String nombre){
        
        return "";
    }
    
    public String consultarEmpreaCategoria(Categoria categoria){
        
        return"";
    }
    
    public String consultarSectorCiudad(Sector sector){;
        
        return "";
    }
    
    
    
     private int generarId(){
        return ++Usuario.cont;
        
    }
     public int getCodigo() {
        return ++Usuario.cont;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public void setCiudadNacimiento(String ciudadNacimiento) {
        this.ciudadNacimiento = ciudadNacimiento;
    }

    public void setTipoTarjeta(String[] tipoTarjeta) {
        this.tipoTarjeta = tipoTarjeta;
    }

    @Override
    public String toString() {
        return "Usuario{" + "fechaNacimiento=" + fechaNacimiento + ", ciudadNacimiento=" + ciudadNacimiento + ", tipoTarjeta=" + tipoTarjeta + ", Codigo=" + Codigo + '}';
    }
    
     


    

   

    
}
