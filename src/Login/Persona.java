/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Login;

/**
 *
 * @author Romy
 */
public abstract class Persona implements java.io.Serializable{
    private String nombreCompleto; 
    private String Correo;
    private String Password;
    

    public Persona(String nombreCompleto, String Correo, String Password) {
        this.nombreCompleto = nombreCompleto;
        this.Correo = Correo;
        this.Password = Password;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public String getCorreo() {
        return Correo;
    }

    public String getPassword() {
        return Password;
    }
        
    public abstract void presentarDatos();

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    @Override
    public String toString() {
        return "Persona{" + "nombreCompleto=" + nombreCompleto + ", Correo=" + Correo + ", Password=" + Password + '}';
    }
    

    
}
