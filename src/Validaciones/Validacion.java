/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validaciones;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

/**
 *
 * @author Romy
 */
public class Validacion {
    public static Scanner sc = new Scanner(System.in);

    
    public String validacionIntMenu() {
        String valor = null;
        boolean parametro=true;
        while (parametro) {
            String cadena=sc.nextLine();
            try {
                Integer.parseInt(cadena);
                parametro=false;
                valor=cadena;
            } catch (NumberFormatException e) {
                System.out.print("\nLa opcion ingresada no es valida\nPor favor vuelva a ingresar: ");
            }catch (Exception e) {
                System.out.print("\nLa opcion ingresada no es valida\nPor favor vuelva a ingresar: ");
            }
        }
        return valor;
    }
    public String siornot() {
        String valor = "";
        while (!valor.equals("si") && !valor.equals("no")) {
            System.out.print("Desea salir del Sistema:(Si/No)");
            valor = sc.nextLine().toLowerCase();
        }
        if ("si".equals(valor)){
            return valor;
        }
        else{
            return valor;
        }
    }
    public static boolean validacionTelefono(String  cadena){
        int num;
        try{
            num=Integer.parseInt(cadena);
            return true;
        }catch(Exception e){
            return false;
        }
    }
    public static String[] pedirTelefono(){
        boolean confir;
        System.out.println("Ingrese los telefonos separados por \",\"");
        String telefonos=sc.next();
        sc.nextLine();
        String[] comprobar=telefonos.split(",");
        String[] telefin=new String[comprobar.length];
        int i;
        for(i=0;i<comprobar.length;i++){

            confir=validacionTelefono(comprobar[i]);
            if(confir==true){
                telefin[i]=comprobar[i];
            }else{
                System.out.println("Ingrese correctamente los numeros telefonicos");
                telefonos=sc.nextLine();
                
            }    
            }
        System.out.println(telefin.length);
        return telefin;      
    }
    public static String[] pedirHorario(){
        String[] horario=new String[2];
        System.out.println("Ingrese la hora de apertura del establecimiento formato \"HH:mm\"");
        String hora1=sc.next();
        sc.nextLine();

         while(!confirmarHora(hora1)==true){
                System.out.println("Escriba correctamente la hora de apertura \"HH:mm\"");
                hora1=sc.next();  
                sc.nextLine();

            }
        horario[0]=hora1;
        System.out.println(hora1+"gg");
        System.out.println("Ingrese la hora de cierre del establecimiento formato \"HH:mm\"");
        String hora2=sc.next();
        sc.nextLine();

        while(!confirmarHora(hora2)==true){
                System.out.println("Escriba correctamente la hora de cierre \"HH:mm\"");
                hora2=sc.next();   
                sc.nextLine();

            }
        horario[1]=hora2;
        System.out.println(hora1+"gg");
        
        return horario;

       
        
    }
    
    
        public static boolean confirmarHora(String hora){
         try{
             LocalTime fecha3 = LocalTime.parse(hora);
             System.out.println(hora);
             return true;
                  
             
            
        }catch(Exception e){
           return false;
        }
    }
        
        
  
    
}
